//import restify
var restify = require('restify');
//create Server
var server = restify.createServer();

//import request handler
var request = require('request');
//import moduleJSON from ./moduleJSON
var ExJSON = require('./moduleJSON');

//blocks chain on reading and parsing the HTTP request body
server.use(restify.plugins.bodyParser());
//parse the HTTP query string, parsed data will be avalible at req.query, additional params are merged into req.params
server.use(restify.plugins.queryParser());

//vary port number
var port = server.listen(process.env.PORT || 8080);
server.listen(port,function(){
    // log for initiation
   console.log('Server initiated.');
   
   //setup firebase connection
   //import firebase
   var firebase = require("firebase");
   //setup connection token
   var config = {
                    apiKey: "AIzaSyDJxk-s-zofgTXd9xHHzGeYTjKYgAeutIM",
                    authDomain: "restful-api-20c86.firebaseapp.com",
                    databaseURL: "https://restful-api-20c86.firebaseio.com",
                    projectId: "restful-api-20c86",
                    storageBucket: "restful-api-20c86.appspot.com",
                    messagingSenderId: "70063079558"
                };
    //initialize firebase connection
    firebase.initializeApp(config);
    //simplify the process of calling firebase.database()
    var db = firebase.database();
    
   //GET function
   server.get("/*",function(req,res,next){
       res.header('Access-Control-Allow-Origin','*');
       res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With, yourHeaderFeild');
       res.header('Access-Control-Allow-Methods', 'PUT, POST , GET, DELETE , OPTIONS');
       //log for GET function initialize
       console.log('GET request being processed');
       //define varialbes
       //define the name being GET from query
       var name = req.query.name;
       //setup firebase reference
       var ref = db.ref("/superheros/" + name);
       //read data and listen for snapshot changes
       ref.once("value", function(snapshot) {
       // run when snapshot data is not empty
       if (snapshot.val() != null) {
           //define String for storing snapshot value in string
           var string = JSON.stringify(snapshot.val());
           //parse the snapshot value
           var objectValue = JSON.parse(string);
           //string the parsed value
           var JSONStr = ExJSON(objectValue['name'],objectValue['description']);
           //define object reqStatus
           var reqStatus = {
               "Status" : "GET request finished",
               "Content" : JSONStr
           };
           //log for successful GET
           console.log(reqStatus);
           //convert js object into json string
           var reqStatusStr = JSON.stringify(reqStatus);
           //send the response object --> reqStatusStr
           res.send(reqStatusStr);
           //end the response
           res.end();
       } else { //run when snapshot data is empty
           request('http://superheroapi.com/api/1869999813050434/search/'+name, function (error, response, body) {//request data from third-party api(Superheroapi)
                           //parse the json retrieved
                            var json = JSON.parse(body);
                            //if there is no error in json
                            if (!("error" in json)){
                                //String the name and the data retrieved
                                var JSONStr = ExJSON(name,json.results[0].work.occupation);
                                //define object reqStatus
                                var reqStatus = {
                                    "Status" : "GET request redirected",
                                    "Message" : "database does not contain data of "+ name+ ", we found it from SuperHero API instead.",
                                    "Content" : JSONStr
                                }
                                //log for redirected GET
                                console.log(reqStatus);
                                //convert js object into json string
                                var reqStatusStr = JSON.stringify(reqStatus);
                                //send the response object --> reqStatusStr
                                res.send(reqStatusStr);
                                //end the response
                                res.end();
                                //define the reference to firebase
                                var ref = db.ref("/superheros/"+name);
                                //define the content to be passed to firebase
                                var content = {
                                    name: name,
                                    description: json.results[0].work.occupation
                                }
                                //set the content in firebase
                                ref.set(content);
                            } else {
                                //if there is no data from SuperHero api then search in wikipedia api
                                request('https://en.wikipedia.org/w/api.php?action=parse&section=0&prop=text&page='+name+'&format=json', function (error, response, body) {
                                    //parse the json retrieved
                                    var json = JSON.parse(body);
                                    //if theres no error in json
                                    if (!("error" in json)){
                                        //String the name and the data retrieved
                                        var JSONStr = ExJSON(name,json['parse']['text']['*']);
                                        //define object reqStatus
                                        var reqStatus = {
                                            "Status" : "GET request redirected",
                                            "Message" : "database and SuperHero api do not have the information for "+name+", request is being redirected to wikipedia api" ,
                                            "Content" : JSONStr
                                        };
                                        //log for redirected GET
                                        console.log(reqStatus);
                                        //convert js object into json string
                                        var reqStatusStr = JSON.stringify(reqStatus);
                                        //send the response object --> reqStatusStr
                                        res.send(reqStatusStr);
                                        //end the response
                                        res.end();
                                        //define the reference to firebase
                                        var ref = db.ref("/superheros/"+name);
                                        //define the content to be passed to firebase
                                        var content = {
                                            name: name,
                                            description: json['parse']['text']['*']
                                        }
                                        //set the content in firebase
                                        ref.set(content);
                                    } else {
                                        //request from both api failed
                                        console.log("failed");
                                        //define object reqStatus
                                        var reqStatus = {
                                            "Status" : "GET request failed",
                                            "Message" : "thers no such thing as " +name,
                                            "Content" : JSONStr
                                        }
                                        //log for redirected GET
                                        console.log(reqStatus);
                                        //convert js object into json string
                                        var reqStatusStr = JSON.stringify(reqStatus);
                                        //send the response object --> reqStatusStr
                                        res.send(reqStatusStr);
                                        //end the response
                                        res.end();
                                        
                                    }
                                    
                                });
                            }
               
           });
           
       } 
       });
    });
   
   //POST function
   server.post("/",function(req,res,next){
       res.header('Access-Control-Allow-Origin','*');
       res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With, yourHeaderFeild');
       res.header('Access-Control-Allow-Methods', 'PUT, POST , GET, DELETE , OPTIONS');
       //log for initialize post request
       console.log('POST request being processed');
       //define the variables
       //define the name being POST from query
       var name = req.body.name;
       //define the superpower being POST from query
       var description = req.body.description;
       //define the actor being POST
       //setup the reference in firebase
       var ref = db.ref("/superheros/"+name);
       //define object value
       var content = {
        name: name,
        description: description,
       }
       //write "content" into firebase
       ref.set(content);
       //define object reqStatus
       var reqStatus = {
           "Status" : "POST completed",
           "Message" : name + " is now a new superhero."
       };
       //log for successful POST
       console.log(reqStatus);
       //convert js object into json string
       var reqStatusStr = JSON.stringify(reqStatus);
       //send the reponse object --> reqStatusStr
       res.send(reqStatusStr);
       //end the response
       res.end();
   });
   
   //DEL function
   server.del("/*",function(req,res,next){
       res.header('Access-Control-Allow-Origin','*');
       res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With, yourHeaderFeild');
       res.header('Access-Control-Allow-Methods', 'PUT, POST , GET, DELETE , OPTIONS');
       //log for DEL function initialized
       console.log('DEL request being processed');
       //define variables
       //define the name being DEL from query
       var name = req.query.name;
       //setup the reference in firebase
       var ref = db.ref("/superheros/"+name);
       //read data and listen for snapshot changes
       ref.once("value", function(snapshot) {
       //run when snapshot value is not empty
       if (snapshot.val() != null) {
           //remove the name of the object in firebase
           ref.remove();
           //define object reqStatus
           var reqStatus = {
               "Status" : "DEL completed",
               "Message" : name + " is being moved away from the fight."
           };
           //log for successful DEL
           console.log(reqStatus);
           //convert js object into json string
           var reqStatusStr = JSON.stringify(reqStatus);
           //send the response object --> reqStatusStr
           res.send(reqStatusStr);
           //end the response
           res.end();
       } else { //run when snapshot data is empty
           //define object reqStatus
           var reqStatus = {
                "Status" : "ERROR",
                "Message" : name + " wasn't even there."
           };
           //log for unsuccessful DEL
           console.log(reqStatus);
           //convert js object into json string
           var reqStatusStr = JSON.stringify(reqStatus);
           //send the response object --> reqStatusStr
           res.send(reqStatusStr);
           //end the response
           res.end();
        }
       });
    });
});